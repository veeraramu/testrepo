import { Component, OnInit } from '@angular/core';
import {analysis,analysismd} from '../shared/analysis'
import { Observable, throwError } from 'rxjs';
import { catchError, filter, retry } from 'rxjs/operators';
// import { Injectable } from '@angular/core';
import * as FusionCharts from "fusioncharts";
import { HttpClient } from '@angular/common/http';
import { HttpHeaders} from '@angular/common/http';
import { homeService } from '../shared/home.service';
import { ColDef } from 'ag-grid-community';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
//@Injectable()
export class HomeComponent implements OnInit {
  configUrl:string = "https://localhost:5001/api/Analysis/WeekDay";
  AnalysisData : analysis[] | undefined;
  AnalysismdData: analysismd[]| undefined;
  dataSource: any;
  datamdSource:any;
  dataFormat = "json";
  type: string;
  width: string;
  height: string;
  rowData: any;
  rowmdData: any;
  gridData:any =[];
  gridmdData:any =[];
  columnDefs: ColDef[] = [
      { 
        field: 'weekOfYear', 
        filter: true,
        sortable: true
      },
      { field: 'start_and_Date' },
      { field: 'noofapps_M_to_S' },
      { 
        field: 'avgnoofapps',
        sortable: true
      }
   ];
   columnmdDefs: ColDef[] = [
      { 
        field: 'month', 
        filter: true,
        sortable: true
      },
      { field: 'noOfApplications' },
      { field: 'average',
        filter:true
      },
   ];
  constructor(private http: HttpClient,private _homeService:homeService) { 
    this.type = 'line';
    this.width = '800';
    this.height = '450';
    this.dataSource = {
      chart: {
        caption: "Weekday Analysis Data",
        subcaption: "Analysis",
        xaxisname: "Week No.",
        yaxisname: "No Of Apps",
        theme: "fusion"
      },
      data:null
    }
    this.datamdSource = {
      chart: {
        caption: "Monthly Analysis Data",
        subcaption: "Analysis",
        xaxisname: "Month name.",
        yaxisname: "No Of Apps",
        theme: "fusion"
      },
      data:null
    }
    
  }

  ngOnInit(): void {
   this.getConfig();
  }
  getConfig() {
    debugger
  //   const schema = [{
  //     "name": "weekOfYear",
  //     "type": "string",
  // }, {
  //     "name": "avgnoofapps",
  //     "type": "number"
  // }] 
   this._homeService.fetchAnalysisData()
   .subscribe((data: analysis[]) => {
     this.AnalysisData = data
     this.rowData=this.AnalysisData;
     let gt:any = []
    this.AnalysisData.forEach(function(item){
        const st ={
          "label":item.weekOfYear,
          "value":item.avgnoofapps
        }
        gt.push(st)
      });
      this.gridData= gt;
      this.dataSource.data = this.gridData;
      console.log("ts test",this.gridData) 
    });

    this._homeService.fetchMonthlyAnaData()
      .subscribe((data: analysismd[]) => {
        this.AnalysismdData = data
        this.rowmdData=this.AnalysismdData;
        let gt:any = []
        this.AnalysismdData.forEach(function(item){
            const rt ={
              "label":item.month,
              "value":item.average
            }
            gt.push(rt)
        });
        this.gridmdData= gt;
          this.datamdSource.data = this.gridmdData;
          console.log("ts test",this.gridmdData)
        });
    } 

}
