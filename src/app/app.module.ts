import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import {homeService} from "./shared/home.service"
import { AgGridModule } from 'ag-grid-angular';
import { FusionChartsModule } from "angular-fusioncharts";

// Import FusionCharts library and chart modules
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as TimeSeries from 'fusioncharts/fusioncharts.timeseries';
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import { LayoutComponent } from './layout/layout.component';
FusionChartsModule.fcRoot(FusionCharts,charts, FusionTheme);
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FusionChartsModule,
    AgGridModule.withComponents([])
  ],
  providers: [homeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
