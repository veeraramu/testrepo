import { Injectable, Injector  } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { HttpHeaders} from '@angular/common/http';
import { analysis,analysismd } from "./analysis";

@Injectable()
export class homeService {
    configUrlwd:string = "https://localhost:5001/api/Analysis/WeekDay";
    configUrlmd:string = "https://localhost:5001/api/Analysis/month";
    constructor(private http:HttpClient){}
    fetchAnalysisData():Observable<analysis[]>{
        const headers = new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('Access-Control-Allow-Headers', 'Content-Type')
        .append('Access-Control-Allow-Methods', 'GET')
        .append('Access-Control-Allow-Origin', '*');
        return this.http.get<analysis[]>(this.configUrlwd, {headers})
    }
    fetchMonthlyAnaData(){
        const headers = new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('Access-Control-Allow-Headers', 'Content-Type')
        .append('Access-Control-Allow-Methods', 'GET')
        .append('Access-Control-Allow-Origin', '*');
        return this.http.get<analysismd[]>(this.configUrlmd, {headers})
    }
}